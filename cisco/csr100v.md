configuration...

Current configuration : 4496 bytes
!
! Last configuration change at 19:43:08 UTC Mon Jul 9 2018 by ec2-user
!
version 16.6
service timestamps debug datetime msec
service timestamps log datetime msec
platform qfp utilization monitor load 80
no platform punt-keepalive disable-kernel-core
platform console virtual
!
hostname ip-172-31-44-193
!
boot-start-marker
boot-end-marker
!
!
logging persistent size 1000000 filesize 8192 immediate
!
no aaa new-model
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
subscriber templating
! 
! 
! 
! 
!
!
!
multilink bundle-name authenticated
!
!
!
!
!
crypto pki trustpoint TP-self-signed-427089055
 enrollment selfsigned
 subject-name cn=IOS-Self-Signed-Certificate-427089055
 revocation-check none
 rsakeypair TP-self-signed-427089055
!
!
redundancy
!
! 
!
!
interface VirtualPortGroup0
 ip address 192.168.35.1 255.255.255.0
 ip nat inside
 no mop enabled
 no mop sysid
!
interface GigabitEthernet1
 ip address dhcp
 ip nat outside
 negotiation auto
 no mop enabled
 no mop sysid
!
router bgp 65500
 bgp router-id 172.31.44.193
 bgp log-neighbor-changes
 bgp listen range 10.12.0.0/16 peer-group CAAS
 neighbor CAAS peer-group
 neighbor CAAS remote-as 65520
 neighbor CAAS ebgp-multihop 255
 neighbor CAAS update-source GigabitEthernet1
 !
 address-family ipv4
  neighbor CAAS activate
  neighbor CAAS soft-reconfiguration inbound
 exit-address-family
!
!
virtual-service csr_mgmt
 ip shared host-interface GigabitEthernet1
 activate
!
iox
ip nat inside source list GS_NAT_ACL interface GigabitEthernet1 overload
ip forward-protocol nd
ip tcp window-size 131072
ip http server
ip http authentication local
ip http secure-server
ip route 0.0.0.0 0.0.0.0 GigabitEthernet1 172.31.32.1
ip route 10.12.0.0 255.255.0.0 172.31.32.1
!
ip ssh window-size 131072
ip ssh rsa keypair-name ssh-key
ip ssh version 2
ip ssh pubkey-chain
  username ec2-user
   key-hash ssh-rsa 12CDEA128CEEA982AD2C67F9C39536C4 ec2-user
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip scp server enable
!
!
ip access-list standard GS_NAT_ACL
 permit 192.0.0.0 0.255.255.255
!
!
!
!
control-plane
!
!
!
!
!
!
line con 0
 stopbits 1
line vty 0 4
 login local
 transport input ssh
!
!
!
!
!
!
end
