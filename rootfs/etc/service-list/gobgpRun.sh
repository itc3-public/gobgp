#!/bin/bash

if [ "$DEBUG" = "true" ]; then
exec > >(tee -ia /bgp-gobgpd-script.log)
exec 2> >(tee -ia /bgp-gobgpd-script.log >&2)
fi

exec > >(tee -ia /bgp-gobgpd-script.log)
exec 2> >(tee -ia /bgp-gobgpd-script.log >&2)

while [ ! -f /gobgp.json ]
do
cp /bgpConfig/config.json /gobgp.json
# Determine which ebgp speaker to peer to
routerId=$(ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)

lastBit=$(ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1 | cut -d '.' -f4-)

if [ $((lastBit%2)) -eq 0 ];
then
    echo "even";
    sed -i "s~##IBGP_PEER##~10.12.0.16~" /gobgp.json
    sed -i "s~##ROUTER_ID##~$routerId~" /gobgp.json
else
    echo "odd";
    sed -i "s~##IBGP_PEER##~10.12.0.17~" /gobgp.json
    sed -i "s~##ROUTER_ID##~$routerId~" /gobgp.json
fi
done

exec /usr/bin/gobgpd --graceful-restart --log-plain -t json -f /gobgp.json
sleep 999d