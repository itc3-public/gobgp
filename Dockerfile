FROM alpine:latest
MAINTAINER admin@itc3.io

COPY rootfs /

RUN apk add --no-cache curl bash quagga diffutils runit inotify-tools jq ca-certificates \
&& wget https://github.com/osrg/gobgp/releases/download/v1.31/gobgp_1.31_linux_amd64.tar.gz \
&& mkdir -p /etc/service \
&& tar zxvf ./gobgp_1.31_linux_amd64.tar.gz \
&& mv gobgpd /usr/bin/gobgpd \
&& chmod +x /usr/bin/gobgpd \
&& mv gobgp /usr/bin/bgp \
&& chmod +x /usr/bin/bgp \
&& chmod +x /sbin/runit-docker

ENTRYPOINT ["/sbin/runit-docker"]

